/**
 * Copyright 2014 Apple, Inc
 * Apple Internal Use Only
 **/


package maven.sonar;


public class test {
    String message = "foo";

    public String foo() {
        return message;
    }

    public void uncoveredMethod() {
        System.out.println(foo());
    }
}
